# BenthOBS Dashboard

The BenthOBS Dashboard is a Dash/Plotly based web application allowing user-friendly visualisation of data collected, analyzed, and published by the French [BenthOBS National Observation Service](https://benthobs.fr) (SNO).

The actual dashboard is available [here](https://data.benthobs.fr/dash)

## Authors and acknowledgment
The main contributors to the project are :

  - Fatima Ezzahraa El Houjjaji: developer
  - Mark Hoebeke: developer, project manager

Funding for the developement of the dashboard has been provided by the [ILICO](http://ir-ilico.fr) Research Infrastructure.

## License
The BenthOBS Dashboard is licensed under AGPL v3.

## Installing & Running the Dashboard

The "official" Dashboard instance is available at [https://data.benthobs.fr/dash](https://data.benthobs.fr/dash).

It is also possible to install a separate instance of the Dashboard on any computer running a Docker server. The official Dashboard Docker image is available at [https://quay.io/repository/mhoebeke/benthobsdash](https://quay.io/repository/mhoebeke/benthobsdash).

The [config/benthobsdash_config.toml](config/benthobsdash_config.toml) can be used to customize the Dashboard configuration.
It is possible to use alternate URLs for the data sources, provided the data format is identical to that of the [original data source](https://data.benthobs.fr/files/latest/all_sites/).

