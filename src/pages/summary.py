import logging

import dash
import pandas as pd
from dash import Dash, html, dcc, callback, Output, Input, ALL, MATCH, State, Patch, ctx
import dash_ag_grid as dag
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.figure_factory as ff
import plotly.graph_objects as go


import benthobs_data_tools as bdt

dash.register_page(__name__,order=1)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

def summary_table_info():

    m_summary_info = bdt.macrofauna_df[['Survey','Sampling date']]

    m_summary_info["Sampling date"] = pd.to_datetime(m_summary_info["Sampling date"])
    m_summary_info["Start"] = m_summary_info["Sampling date"].apply(lambda x: pd.offsets.MonthBegin().rollback(x))
    m_summary_info["Start"] = m_summary_info["Start"].apply(lambda x: x.replace(hour=0, minute=0, second=0))
    m_summary_info["Finish"] = m_summary_info['Sampling date'].apply(lambda x: pd.offsets.MonthEnd().rollforward(x))
    m_summary_info["Finish"] = m_summary_info["Finish"].apply(lambda x: x.replace(hour=0, minute=0, second=0))
    m_summary_info.drop('Sampling date', axis=1, inplace=True)
    m_summary_info=m_summary_info.drop_duplicates(keep='first')
    m_summary_info["Task"] = m_summary_info["Survey"]+"- M"
    m_summary_info["Resource"] = " Macrofauna"

    g_summary_info = bdt.granulometry_df[['Survey','Sampling date']]
    g_summary_info["Sampling date"] = pd.to_datetime(g_summary_info["Sampling date"])
    g_summary_info["Start"] = g_summary_info["Sampling date"].apply(lambda x: pd.offsets.MonthBegin().rollback(x))
    g_summary_info["Start"] = g_summary_info["Start"].apply(lambda x: x.replace(hour=0, minute=0, second=0))
    g_summary_info["Finish"] = g_summary_info['Sampling date'].apply(lambda x: pd.offsets.MonthEnd().rollforward(x))
    g_summary_info["Finish"] = g_summary_info["Finish"].apply(lambda x: x.replace(hour=0, minute=0, second=0))
    g_summary_info.drop('Sampling date', axis=1, inplace=True)
    g_summary_info = g_summary_info.drop_duplicates(keep='first')
    g_summary_info["Task"] = g_summary_info["Survey"]+"-G"
    g_summary_info['Resource'] ='Granulometry'

    summary_info=pd.concat([m_summary_info,g_summary_info])

    summary_info.sort_values(['Task'], inplace=True)

    summary_info.rename(columns={'Task': 'Task', "Start" : "Start", "Finish" : 'Finish'}, inplace=True)

    colors = {
        ' Macrofauna' : 'rgb(0,128,255)',
        'Granulometry': 'rgb(255,128,0)',
    }

    fig = ff.create_gantt(summary_info, colors=colors, index_col="Resource", show_colorbar=True, title="Overview of Sampling Timespan", group_tasks=True)

    return dcc.Graph(figure=fig)

def macrofauna_table_info():
    """
    Create and return a data table displaying macrofauna information.

    :return: The Dash Ag-Grid table for macrofauna information.
    :rtype: dag.AgGrid
    """
    # Calcul des statistiques pour chaque étude
    df_table_info = bdt.macrofauna_df.groupby('Survey').agg({'Sampling date': ['nunique', 'min', 'max'],
                                                         'Accepted taxon name': ['count', 'nunique'],
                                                         'Latitude': 'first', 'Longitude': 'first'})
    df_table_info.columns = ['Samples', 'First sample', 'Latest sample', 'Total occurrences',
                             'Accepted taxons', 'Latitude', 'Longitude']
    df_table_info.reset_index(inplace=True)

    # Création du tableau Dash
    macro_table = dag.AgGrid(
        id='macrofauna-data-table',
        columnDefs=[{"field": col, "headerName": col} for col in df_table_info.columns],
        rowData=df_table_info.to_dict('records'),
        defaultColDef={"resizable": True, "sortable": True, "filter": True},
        columnSize="sizeToFit",
        csvExportParams={
            "fileName": "macrofauna_table.csv",
        },
    )

    return macro_table


def granulometry_table_info():
    """
    Create and return a data table displaying macrofauna information.

    :return: The Dash Ag-Grid table for granulometry information.
    :rtype: dag.AgGrid
    """
    df_ganulo_table_info = bdt.granulometry_df.groupby('Survey').agg(
        {'Sampling date': ['nunique', 'min', 'max'], 'Latitude':'first', 'Longitude':'first'})

    df_ganulo_table_info.columns = ['Samples', 'First sample', 'Latest sample', 'Latitude', 'Longitude']

    df_ganulo_table_info.reset_index(inplace=True)

    # Création du tableau Ag-Grid
    granulo_table = dag.AgGrid(
        id='granulometry-data-table',
        columnDefs=[{"field": col, "headerName": col} for col in df_ganulo_table_info.columns],
        rowData=df_ganulo_table_info.to_dict('records'),
        defaultColDef={"resizable": True, "sortable": True, "filter": True},
        columnSize="sizeToFit",
        csvExportParams={
            "fileName": "granulometry_table.csv",
        },
    )

    return granulo_table


@callback(
    Output("macrofauna-data-table", "exportDataAsCsv"),
    Input("m-csv-button", "n_clicks"),
)
def export_macrofauna_data_as_csv(n_clicks):
    if n_clicks:
        return True
    return False


@callback(
    Output("granulometry-data-table", "exportDataAsCsv"),
    Input("g-csv-button", "n_clicks"),
)
def export_granulometry_data_as_csv(n_clicks):
    if n_clicks:
        return True
    return False



layout = html.Div(
html.Div(
            children=[
                html.Div(
                    children=[
                        html.H4(f"Overview of Available Samples"),
                        summary_table_info(),
                    ]
                ),
                html.Div(
                    children=[
                        html.H4(f"Macrofauna table ({bdt.macrofauna_df['Survey'].nunique()} surveys)"),
                        macrofauna_table_info(),
                        html.Button("Download CSV", id="m-csv-button", n_clicks=0),
                    ]
                ),
                html.Div(
                    children=[
                        html.H4(f"Granulometry table ({bdt.granulometry_df['Survey'].nunique()} surveys)"),
                        granulometry_table_info(),
                        html.Button("Download CSV", id="g-csv-button", n_clicks=0),
                    ]
                ),
            ],className="bd-content-full"
        ),
)
