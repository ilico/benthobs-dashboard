import datetime
import calendar
import functools
import re
import random
import dash_leaflet as dl
from dash import Dash, html, dcc, callback, Output, Input, ALL, MATCH, State, Patch, ctx
import dash
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import os.path
import logging

import benthobs_data_tools as bdt

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

controls_by_index = {}
taxon_layers_by_index = {}

dash.register_page(__name__, path="/", name="Home", order=0)



@callback(
    Output('survey-info-layer', 'children'),
    Output('taxon-layer', 'children'),
    Input('survey-info-layer', 'id'),
    Input('taxon-layer', 'id')
)
def update_layers(survey_info_layer, taxon_layer):
    """
    Update the layers of the map with survey information and taxon markers.

    :param survey_info_layer: The ID of the survey info layer.
    :type survey_info_layer: str
    :param taxon_layer: The ID of the taxon layer.
    :type taxon_layer: str

    :return: A tuple containing the survey info markers and taxon markers.
    :rtype: tuple
    """
    survey_info_markers = []
    taxon_markers = []

    for survey in bdt.macrofauna_df['Survey'].unique():
        survey_data = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey]
        lat, lon = survey_data.iloc[0]['Coordinates']

        survey_info = html.Div([
            html.H4(f"Survey: {survey}"),
            html.P(f"First Sampling Date: {survey_data['Sampling date'].min()}"),
            html.P(f"Last Sampling Date: {survey_data['Sampling date'].max()}"),
            html.P(f"Number of Sampling: {len(survey_data['Sampling date'].unique())}"),
            html.P(f"Coordinates: {lat}, {lon}"),
            html.A([
                html.Button('Download XLSX', id={'type' : 'btn-download-xlsx', 'index' : f"{survey}"}),
                dcc.Download(id={'type' : 'download-xlsx', 'index' : f"{survey}"})
                ]
            ),
            html.A([
                html.Button('Download CSV', id={'type' : 'btn-download-csv', 'index' : f"{survey}"}),
                dcc.Download(id={'type' : 'download-csv', 'index' : f"{survey}"})
                ]
            ),
            html.A([
                html.Button('Download JSON', id={'type' : 'btn-download-json', 'index' : f"{survey}"}),
                dcc.Download(id={'type' : 'download-json', 'index' : f"{survey}"})
                ]
            ),
        ])

        survey_info_marker = dl.Marker(
            position=[lat, lon],
            children=dl.Popup([survey_info], closeOnClick=True),
            title=survey
        )
        survey_info_markers.append(survey_info_marker)

        taxon_names = bdt.survey_taxons[survey]['Accepted taxon name']
        counts = bdt.survey_taxons[survey]['Count']
        colors = bdt.survey_taxons[survey]['Color']

        fig = go.Figure(
            data=[
                go.Pie(
                    labels=taxon_names,
                    values=counts,
                    marker=dict(colors=colors))],
        )
        fig.update_layout(
            height=350,
            width=350,
            margin=dict(l=15, r=15, t=35, b=15),
            legend=dict(orientation='h', x=0.5, y=-0.2),
            title=f"Top 5 taxons in {survey}",
        )

        title = html.H4(f"Survey: {survey}")
        popup_content = html.Div([title, dcc.Graph(figure=fig, )],
                                 style={'width': 'auto', 'height': 'auto'})

        taxon_marker = dl.Marker(
            position=[lat, lon],
            children=dl.Popup([popup_content], closeOnClick=True)
        )
        taxon_markers.append(taxon_marker)

    return survey_info_markers, taxon_markers


@callback(
    Output({'type': 'download-csv', 'index': MATCH}, 'data'),
    Input({'type': 'btn-download-csv', 'index': MATCH}, 'n_clicks'),
    State({'type': 'btn-download-csv', 'index': MATCH}, 'id')
)
def download_csv_for_survey(n_clicks, id):
    """
    Download the macrofauna data for a specific survey in CSV format.

    :param n_clicks: The number of clicks on the download CSV button.
    :type n_clicks: int
    :param id: The ID of the download CSV button.
    :type id: dict

    :return: The CSV file for the specific survey data.
    :rtype: dcc.send_data_frame
    """
    survey = id['index']
    survey_data = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey]
    return dcc.send_data_frame(survey_data.to_csv,f"benthobs-data-{survey}.csv")


@callback(
    Output({'type': 'download-xlsx', 'index': MATCH}, 'data'),
    Input({'type': 'btn-download-xlsx', 'index': MATCH}, 'n_clicks'),
    State({'type': 'btn-download-xlsx', 'index': MATCH}, 'id')
)
def download_xlsx_for_survey(n_clicks, id):
    """
    Download the macrofauna data for a specific survey in XLSX format.

    :param n_clicks: The number of clicks on the download XLSX button.
    :type n_clicks: int
    :param id: The ID of the download XLSX button.
    :type id: dict

    :return: The XLSX file for the specific survey data.
    :rtype: dcc.send_data_frame
    """
    survey = id['index']
    survey_data = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey]
    return dcc.send_data_frame(survey_data.to_excel,f"benthobs-data-{survey}.xlsx")


@callback(
    Output({'type': 'download-json', 'index': MATCH}, 'data'),
    Input({'type': 'btn-download-json', 'index': MATCH}, 'n_clicks'),
    State({'type': 'btn-download-json', 'index': MATCH}, 'id')
)
def download_json_for_survey(n_clicks, id):
    """
    Download the macrofauna data for a specific survey in JSON format.

    :param n_clicks: The number of clicks on the download JSON button.
    :type n_clicks: int
    :param id: The ID of the download JSON button.
    :type id: dict

    :return: The JSON file for the specific survey data.
    :rtype: dcc.send_data_frame
    """
    survey = id['index']
    survey_data = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey]
    return dcc.send_data_frame(survey_data.to_json,f"benthobs-data-{survey}.json")

@callback([
    Output('graph-controls', 'children'),
    Output('remove-graph-button', 'disabled'),
    Output('graph-controls', 'active_tab'),
    ],
    Input('add-macrofauna-graph-button', 'n_clicks'),
    Input('add-granulometry-graph-button', 'n_clicks'),
    Input('add-top-taxon-graph-button', 'n_clicks'),
    Input('remove-graph-button', 'n_clicks'),
    Input('duplicate-graph-button', 'n_clicks'),
    State('graph-controls', 'active_tab'),
    State('graph-controls', 'children'),
)
def add_remove_graph_controls(n_clicks_add_macrofauna, n_clicks_add_granulometry, n_clicks_add_top_taxon, n_clicks_remove,  n_clicks_duplicate, active_tab_index, children):
    """
    Add or remove graph controls based on user actions.

    This callback function handles the addition and removal of graph controls based on the button clicks from the user.
    It updates the graph controls based on the user's interactions and returns the updated children and the disabled state
    of the remove graph button.

    :param n_clicks_add_macrofauna: The number of clicks on the 'Add Macrofauna Graph' button.
    :type n_clicks_add_macrofauna: int
    :param n_clicks_add_granulometry: The number of clicks on the 'Add Granulometry Graph' button.
    :type n_clicks_add_granulometry: int
    :param n_clicks_add_top_taxon: The number of clicks on the 'Add Top Taxon Graph' button.
    :type n_clicks_add_top_taxon: int
    :param n_clicks_remove: The number of clicks on the 'Remove Graph' button.
    :type n_clicks_remove: int
    :param n_clicks_duplicate: The number of clicks on the 'Duplicate Graph' button.
    :type n_clicks_duplicate: int
    :param active_tab_index: The index of the active tab in the graph controls.
    :type active_tab_index: str
    :param children: The current children of the graph controls.
    :type children: list

    :return: A tuple containing the updated children and the disabled state of the remove graph button.
    :rtype: tuple
    """
    button_clicked = ctx.triggered_id

    if n_clicks_add_macrofauna is None:
        n_clicks_add_macrofauna = 0

    if n_clicks_add_granulometry is None:
        n_clicks_add_granulometry = 0

    if n_clicks_add_top_taxon is None:
        n_clicks_add_top_taxon = 0

    if n_clicks_duplicate is None:
        n_clicks_duplicate = 0

    next_index = n_clicks_add_macrofauna + n_clicks_add_granulometry + n_clicks_add_top_taxon + n_clicks_duplicate

    duplicate_type = None
    if button_clicked == 'duplicate-graph-button':
        active_tab_index_int = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index_int]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']
        duplicate_type = controls_by_index[f"index-{active_tab_id_index}"]['type']

    new_active_tab = f"tab-{len(children)}"

    if button_clicked == 'add-macrofauna-graph-button' or duplicate_type == 'macrofauna':
        (children,disabled) = add_remove_macrofauna_controls(next_index, active_tab_index, children, button_clicked)
        return children,disabled,new_active_tab

    if button_clicked == 'add-granulometry-graph-button' or duplicate_type == 'granulometry':
        (children,disabled) = add_remove_granulometry_controls(next_index, active_tab_index, children, button_clicked)
        return children,disabled,new_active_tab

    if button_clicked is None or button_clicked == 'add-top-taxon-graph-button' or duplicate_type == 'top_taxon':
        (children,disabled) =  add_remove_top_taxon_controls(next_index,active_tab_index,children,button_clicked)
        return children,disabled,new_active_tab

    if button_clicked == 'remove-graph-button' and active_tab_index:
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        LOGGER.info(f"Remove Graph Controls - Active index: {active_tab_id}")
        patched_children = []
        disabled = True
        for child in children:
            child_id = child['props']['id']
            LOGGER.info(f"Child id {child_id}")
            if child_id['type'] == 'graph-control-tab' and child_id['index'] == active_tab_id['index']:
                continue
            patched_children.append(child)
        if len(patched_children) > 1:
            disabled = False
        if active_tab_index < len(patched_children) :
            new_active_tab = f"tab-{active_tab_index}"
        else :
            new_active_tab = f"tab-{len(patched_children)-1}"

        return patched_children, disabled,new_active_tab


def add_remove_macrofauna_controls(next_index,active_tab_index,children,button_clicked):
    """
    Generate controls for Macrofauna graphs.

    :param next_index: The next index for the new tab.
    :param active_tab_index: The index of the active tab.
    :param children: List of children components in the layout.
    :param button_clicked: The identifier of the button that was clicked.
    :return: A tuple containing the updated children components and a disabled flag.
    """
    disabled = True

    LOGGER.info(f"Generating Macrofauna controls at index {next_index}")

    new_control_values = {'type' : 'macrofauna'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(bdt.macrofauna_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    survey_df = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey_value]
    taxon_options = sorted(survey_df['Accepted taxon name'].unique())
    taxon_value = [taxon_options[random.randrange(len(taxon_options) - 1)]]
    new_control_values['taxons'] = taxon_value

    start_date_value = min(bdt.macrofauna_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(bdt.macrofauna_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'sample'
    new_control_values['aggregation'] = aggregation_value


    if button_clicked == 'duplicate-graph-button' :
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index= active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        survey_df = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey_value]
        taxon_options = sorted(survey_df['Accepted taxon name'].unique())
        taxon_value = controls_by_index[f"index-{active_tab_id_index}"]['taxons']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
            id={"type": "macrofauna-survey-dropdown", "index": next_index},
            options=survey_options,
            value=survey_value,
            style={'color': '#000'}

    )

    new_survey_dropdown = html.Div([
        dbc.Label("Survey"),
        survey_select
    ])

    taxon_options = [ taxon_name.replace("<i>","").replace("</i>","") for taxon_name in taxon_options]
    taxon_value = [ taxon_name.replace("<i>","").replace("</i>","") for taxon_name in taxon_value]
    taxon_dropdown = dcc.Dropdown(
            id={"type": "macrofauna-taxon-dropdown", "index": next_index},
            options=taxon_options,
            multi=True,
            value=taxon_value,
            style={'color': '#000'}
        )
    new_taxon_dropdown = html.Div([
        dbc.Label("Taxon :"),
        taxon_dropdown,
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'macrofauna-survey-date-picker', "index": next_index},
            min_date_allowed=min(bdt.macrofauna_df['Sampling date']),
            max_date_allowed=max(bdt.macrofauna_df['Sampling date']),
            initial_visible_month=max(bdt.macrofauna_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'macrofauna-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Box plot', 'value': 'box'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value,
            style={'color': '#000'}
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type":'macrofauna-survey-aggregation',"index": next_index},
            options=[
                {'label': 'Sample', 'value': 'sample'},
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_taxon_dropdown,
            new_date_picker,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Macrofauna Graph {next_index + 1}",
    )

    patched_children.append(new_tab)

    return patched_children, disabled


def add_remove_granulometry_controls(next_index,active_tab_index,children,button_clicked):
    """
    Generate controls for Granulometry graphs.

    :param next_index: The next index for the new tab.
    :param active_tab_index: The index of the active tab.
    :param children: List of children components in the layout.
    :param button_clicked: The identifier of the button that was clicked.
    :return: A tuple containing the updated children components and a disabled flag.
    """
    disabled = True

    LOGGER.info(f"Generating Granulometry controls at index {next_index}")

    new_control_values = {'type' : 'granulometry'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(bdt.granulometry_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    start_date_value = min(bdt.granulometry_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(bdt.granulometry_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'stacked histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'month per year'
    new_control_values['aggregation'] = aggregation_value

    if button_clicked == 'duplicate-graph-button' :
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index= active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']

    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
            id={"type": "granulometry-survey-dropdown", "index": next_index},
            options=survey_options,
            multi=True,
            value=survey_value)

    new_survey_dropdown = html.Div([
        dbc.Label("Granulometry Survey"),
        survey_select
    ])

    new_date_picker = html.Div([
        dbc.Label('G-Date :'),
        dcc.DatePickerRange(
            id={"type": 'granulometry-survey-date-picker', "index": next_index},
            min_date_allowed=min(bdt.granulometry_df['Sampling date']),
            max_date_allowed=max(bdt.granulometry_df['Sampling date']),
            initial_visible_month=max(bdt.granulometry_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value,
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Granulometry Graph Type :'),
        dcc.Dropdown(
            id={"type": 'granulometry-graph-type', "index": next_index},
            options=[
#                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Stacked histogram', 'value': 'stacked histogram'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('G-Aggregation per :'),
        dcc.RadioItems(
            id={"type":'granulometry-survey-aggregation',"index": next_index},
            options=[
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'},
                {'label': 'Season per year', 'value': 'season per year'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_date_picker,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Granulometry Graph {next_index + 1}",
    )

    patched_children.append(new_tab)

    return patched_children, disabled


def add_remove_top_taxon_controls(next_index,active_tab_index,children,button_clicked):
    """
    Generate controls for Top N Taxons graphs.

    :param next_index: The next index for the new tab.
    :param active_tab_index: The index of the active tab.
    :param children: List of children components in the layout.
    :param button_clicked: The identifier of the button that was clicked.
    :return: A tuple containing the updated children components and a disabled flag.
    """
    disabled = True

    LOGGER.info(f"Generating Top N Taxons controls at index {next_index}")

    new_control_values = {'type': 'top_taxon'}
    if children and len(children) > 0:
        disabled = False

    patched_children = Patch()

    survey_options = sorted(bdt.macrofauna_df['Survey'].unique())
    survey_value = survey_options[random.randrange(len(survey_options) - 1)]
    new_control_values['survey'] = survey_value

    survey_df = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey_value]
    max_top_taxons = len(survey_df['Accepted taxon name'].unique())
    top_taxons_value = min(10,max_top_taxons)
    new_control_values['top_taxon'] = top_taxons_value

    start_date_value = min(bdt.macrofauna_df['Sampling date'])
    new_control_values['start_date'] = start_date_value
    end_date_value = max(bdt.macrofauna_df['Sampling date'])
    new_control_values['end_date'] = end_date_value

    graph_type_value = 'histogram'
    new_control_values['graph_type'] = graph_type_value

    aggregation_value = 'year'
    new_control_values['aggregation'] = aggregation_value

    computation_method_value = 'cumulative'
    new_control_values['computation_method'] = computation_method_value

    if button_clicked == 'duplicate-graph-button':
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index = active_tab_id['index']

        survey_value = controls_by_index[f"index-{active_tab_id_index}"]['survey']
        survey_df = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey_value]
        max_top_taxons = len(survey_df['Accepted taxon name'].unique())
        top_taxons_value = controls_by_index[f"index-{active_tab_id_index}"]['top_taxon']
        start_date_value = controls_by_index[f"index-{active_tab_id_index}"]['start_date']
        end_date_value = controls_by_index[f"index-{active_tab_id_index}"]['end_date']
        computation_method_value = controls_by_index[f"index-{active_tab_id_index}"]['computation_method']
        graph_type_value = controls_by_index[f"index-{active_tab_id_index}"]['graph_type']
        aggregation_value = controls_by_index[f"index-{active_tab_id_index}"]['aggregation']


    controls_by_index[f"index-{next_index}"] = new_control_values

    survey_select = dcc.Dropdown(
        id={"type": "top-taxon-survey-dropdown", "index": next_index},
        options=survey_options,
        value=survey_value)

    new_survey_dropdown = html.Div([
        dbc.Label("Survey"),
        survey_select
    ])

    new_top_taxon_count = html.Div([
        dbc.Label('Top N Taxon:'),
        dbc.Input(
            id={"type": 'top-taxon-input', "index": next_index},
            type='number',
            value=top_taxons_value,
            min=1,
            max=max_top_taxons
        ),
    ])

    new_date_picker = html.Div([
        dbc.Label('Date :'),
        dcc.DatePickerRange(
            id={"type": 'top-taxon-survey-date-picker', "index": next_index},
            min_date_allowed=min(bdt.macrofauna_df['Sampling date']),
            max_date_allowed=max(bdt.macrofauna_df['Sampling date']),
            initial_visible_month=max(bdt.macrofauna_df['Sampling date']),
            start_date=start_date_value,
            end_date=end_date_value,
        ),
    ])

    new_computation_method_choice = html.Div([
        dbc.Label('Computation Method :'),
        dcc.Dropdown(
            id={"type": 'top-taxon-computation-method', "index": next_index},
            options=[
                {'label': 'Average', 'value': 'average'},
                {'label': 'Cumulative', 'value': 'cumulative'},
            ],
            value=computation_method_value
        ),
    ])

    new_graph_type_choice = html.Div([
        dbc.Label('Graph Type :'),
        dcc.Dropdown(
            id={"type": 'top-taxon-graph-type', "index": next_index},
            options=[
                {'label': 'Histogram', 'value': 'histogram'},
                {'label': 'Stacked histogram', 'value': 'stacked histogram'},
                {'label': 'Box plot', 'value': 'box'},
                {'label': 'Pie chart', 'value': 'pie'}
            ],
            value=graph_type_value
        ),
    ])

    new_agregation_choice = html.Div([
        dbc.Label('Aggregation per :'),
        dcc.RadioItems(
            id={"type":'top-taxon-survey-aggregation',"index": next_index},
            options=[
                {'label': 'Year', 'value': 'year'},
                {'label': 'Month', 'value': 'month'},
                {'label': 'Month per year', 'value': 'month per year'},
                {'label': 'Season', 'value': 'season'}

            ],
            value=aggregation_value
        ),
    ])

    new_tab = dbc.Tab(
        id={'type': "graph-control-tab", 'index': next_index},
        children=[
            new_survey_dropdown,
            new_top_taxon_count,
            new_date_picker,
            new_computation_method_choice,
            new_graph_type_choice,
            new_agregation_choice,
        ],
        label=f"Top {next_index + 1}")

    patched_children.append(new_tab)

    return patched_children, disabled


@callback(
    Output('graph-panels', 'children'),
    Input('add-macrofauna-graph-button', 'n_clicks'),
    Input('add-granulometry-graph-button', 'n_clicks'),
    Input('add-top-taxon-graph-button', 'n_clicks'),
    Input('remove-graph-button', 'n_clicks'),
    Input('duplicate-graph-button', 'n_clicks'),
    Input({'type': 'graph-up-button', 'index' : ALL}, 'n_clicks'),
    Input({'type': 'graph-down-button', 'index' : ALL}, 'n_clicks'),
    State('graph-controls', 'active_tab'),
    State('graph-panels', 'children'),
    State('graph-controls', 'children')
)
def add_remove_graph_panel(n_clicks_add_macrofauna, n_clicks_add_granulometry, n_clicks_add_top_taxon, n_clicks_remove,  n_clicks_duplicate, n_clicks_graph_up, n_clicks_graph_down,active_tab_index, children, tabs):
    """
    Generate controls for adding or removing graph panels and handle graph reordering.

    :param n_clicks_add_macrofauna: Number of clicks on the 'add-macrofauna-graph-button'.
    :param n_clicks_add_granulometry: Number of clicks on the 'add-granulometry-graph-button'.
    :param n_clicks_add_top_taxon: Number of clicks on the 'add-top-taxon-graph-button'.
    :param n_clicks_remove: Number of clicks on the 'remove-graph-button'.
    :param n_clicks_duplicate: Number of clicks on the 'duplicate-graph-button'.
    :param n_clicks_graph_up: Number of clicks on the 'graph-up-button' for all indices.
    :param n_clicks_graph_down: Number of clicks on the 'graph-down-button' for all indices.
    :param active_tab_index: Index of the active tab.
    :param children: List of children components in the layout.
    :param tabs: Children components of 'graph-controls'.
    :return: The updated children components for 'graph-panels'.
    """
    button_clicked = ctx.triggered_id

    LOGGER.info(f"Button clicked {button_clicked}")

    if n_clicks_add_macrofauna is None:
        n_clicks_add_macrofauna = 0

    if n_clicks_add_granulometry is None:
        n_clicks_add_granulometry = 0

    if n_clicks_add_top_taxon is None:
        n_clicks_add_top_taxon = 0

    if n_clicks_duplicate is None:
        n_clicks_duplicate = 0

    next_index = n_clicks_add_macrofauna + n_clicks_add_granulometry + n_clicks_add_top_taxon + n_clicks_duplicate

    new_graph_type = None
    if button_clicked == 'add-macrofauna-graph-button' :
        new_graph_type = 'macrofauna'
    if button_clicked == 'add-granulometry-graph-button' :
        new_graph_type = 'granulometry'
    if button_clicked is None or button_clicked == 'add-top-taxon-graph-button':
        new_graph_type = 'top_taxon'

    if button_clicked == 'duplicate-graph-button' :
        active_tab_index_int = int(re.sub('tab-', '', active_tab_index))
        active_tab = children[active_tab_index_int]
        active_tab_id = active_tab['props']['id']
        active_tab_id_index= active_tab_id['index']
        new_graph_type = controls_by_index[f"index-{active_tab_id_index}"]['type']

    if new_graph_type is not None:
        new_graph_id = {'type': f"{new_graph_type}-graph", 'index': next_index}
        new_taxon_count_graph = dbc.Col(html.Div([
            dcc.Graph(
                id=new_graph_id
            ),
        ]))

        patched_children = Patch()
        button_div = html.Div(
            className="h-100 d-flex align-items-center justify-content-center",
            children=[
                dbc.Button(
                    id={'type': 'graph-up-button', 'index': next_index},
                    className="bi bi-arrow-up",
                    color="btn btn-light btn-sm"
                ),
                dbc.Button(
                    id={'type': 'graph-down-button', 'index': next_index},
                    className="bi bi-arrow-down",
                    color="btn btn-light btn-sm"
                ),
                dbc.Tooltip(
                    "Move Graph Up",
                    target={'type': 'graph-up-button', 'index': next_index},
                ),
                dbc.Tooltip(
                    "Move Graph Down",
                    target={'type': 'graph-down-button', 'index': next_index},
                )

            ])
        new_graph_panel = dbc.Row(
            id={'type': 'graph-row', 'index': next_index},
            children=[new_taxon_count_graph, button_div],
        )
        patched_children.append(new_graph_panel)
    if button_clicked == 'remove-graph-button' and active_tab_index:
        active_tab_index = int(re.sub('tab-', '', active_tab_index))
        active_tab = tabs[active_tab_index]
        active_tab_id = active_tab['props']['id']
        LOGGER.info(f"Remove Graph Panel - Active index: {active_tab_id}")
        patched_children = []
        for child in children:
            child_id = child['props']['id']
            LOGGER.info(f"Child id {child_id}")
            if child_id['type'] == 'graph-row' and child_id['index'] == active_tab_id['index']:
                continue
            patched_children.append(child)

    if button_clicked is not None and 'type' in button_clicked and (button_clicked['type'] == 'graph-up-button' or button_clicked['type'] == 'graph-down-button'):
        patched_children = children
        button_index = button_clicked['index']
        source_index = None
        source_child = None
        for index in range(0,len(children)):
           child = children[index]
           child_id = child['props']['id']
           if child_id['type'] == 'graph-row' and child_id['index'] == button_index:
               source_index = index
               source_child = child

        if source_child :
            destination_index = source_index
            if button_clicked['type'] == 'graph-up-button':
                destination_index = source_index-1
            if button_clicked['type'] == 'graph-down-button':
                destination_index = source_index+1

            LOGGER.info(f"Attempting to move graph from {source_index} to {destination_index}")
            if source_index >= 0 and source_index < len(children) and destination_index >= 0 and destination_index < len(children):
                destination_child = children[destination_index]
                children[destination_index] = source_child
                children[source_index] = destination_child
            else:
                LOGGER.info(f"Indexes are outside boundaries [0,{len(children)-1}]. No changes performed.")
    return patched_children


@callback(
    Output({'type': 'macrofauna-taxon-dropdown', 'index': MATCH}, 'options'),
    Input({'type': 'macrofauna-survey-dropdown', 'index': MATCH}, 'value'),
    State({'type': 'macrofauna-survey-dropdown', 'index': MATCH}, 'id')
)
def update_taxon_dropdown(survey, id):
    """
    Update the taxon dropdown options based on the selected survey value.

    :param survey: Selected survey value from the dropdown.
    :param id: ID of the macrofauna survey dropdown.
    :return: List of updated taxon dropdown options.
    """
    survey_df = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey]
    taxon_names = [ taxon_name.replace("<i>","").replace("</i>","") for taxon_name in sorted(survey_df['Accepted taxon name'].unique())]
    return taxon_names


@callback(
    Output({'type': 'granulometry-mesh-size-dropdown', 'index': MATCH}, 'options'),
    Input({'type': 'granulometry-survey-dropdown', 'index': MATCH}, 'value'),
    State({'type': 'granulometry-survey-dropdown', 'index': MATCH}, 'id')
)
def update_mesh_size_dropdown(survey, id):
    """
    Update the taxon dropdown options based on the selected survey value.

    :param survey: Selected survey value from the dropdown.
    :param id: ID of the macrofauna survey dropdown.
    :return: List of updated taxon dropdown options.
    """
    survey_df = bdt.granulometry_df[bdt.granulometry_df['Survey'] == survey]
    return sorted(survey_df['Mesh size'].unique())


@callback(
    Output({"type": 'top-taxon-input', "index": MATCH}, 'max'),
    Input({"type": 'top-taxon-survey-dropdown', "index": MATCH}, 'value'),
    State({"type": 'top-taxon-input', "index": MATCH}, 'id')
)
def update_top_taxon_count_max(survey, id):
    """
    Update the maximum value of the top taxon count input based on the selected survey value.

    :param survey: Selected survey value from the dropdown.
    :param id: ID of the top taxon survey dropdown.
    :return: Updated maximum value for the top taxon count input.
    """
    survey_df = bdt.macrofauna_df[bdt.macrofauna_df['Survey'] == survey]
    return len(survey_df['Accepted taxon name'].unique())


@callback(
    Output({'type': 'macrofauna-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'macrofauna-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'macrofauna-taxon-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'macrofauna-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'macrofauna-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'macrofauna-survey-aggregation', 'index': MATCH}, 'value'),
    Input({"type": 'macrofauna-graph-type', "index": MATCH}, 'value'),
    State({'type': 'macrofauna-taxon-dropdown', 'index': MATCH}, 'id')
)
def update_macrofauna_graph(survey, selected_taxa, start_date, end_date, aggregation, graph_type, id):
    """
    Update the macrofauna graph based on the selected survey, taxon, date range, aggregation, and graph type.

    :param survey: Selected survey value from the dropdown.
    :param selected_taxa: List of selected taxon names from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param id: ID of the macrofauna taxon dropdown.
    :return: Updated macrofauna graph figure.
    """

    # Filter the dataframe based on the selected taxonomic groups and date range
    if selected_taxa is None:
        selected_taxa=[]
    else:
        selected_taxa = [ f"<i>{taxon_name}</i>" for taxon_name in selected_taxa]

    if start_date is None:
        start_date = datetime.date(1900,1,1)
    else:
        start_date = datetime.datetime.strptime(start_date,"%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900,12,31)
    else:
        end_date = datetime.datetime.strptime(end_date,"%Y-%m-%d").date()

    LOGGER.info(f"Date type: {type(start_date)}, {start_date}")

    if survey is None:
        survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = survey
        controls_by_index[f"index-{control_index}"]['taxons'] = selected_taxa
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")

    filtered_df = bdt.macrofauna_df[
        (bdt.macrofauna_df['Survey'] == survey) &
        (bdt.macrofauna_df['Sampling datetime'].dt.date >= start_date) &
        (bdt.macrofauna_df['Sampling datetime'].dt.date <= end_date) &
        (bdt.macrofauna_df['Accepted taxon name'].isin(selected_taxa))
        ].copy()

    if graph_type == 'pie':
        # Group the dataframe by taxon name and take the mean of the counts.
        pie_data = filtered_df.groupby('Accepted taxon name', as_index=False)['Count'].mean()
        fig = px.pie(pie_data, names='Accepted taxon name',
                     values='Count',
                     title=f"Macrofauna Graph {graph_type} {id['index'] + 1}: Avg. number of individuals per m² in survey {survey}",)

    elif graph_type == 'box':
        time_group='Sampling date'
        if aggregation == 'year':
            time_group = 'Year'
        elif aggregation == 'month':
            time_group = 'Month'
        elif aggregation == 'month per year':
            time_group = 'Year month'
        elif aggregation == 'season':
            time_group = 'Season'
        fig = px.box(
            filtered_df,
            x=time_group,
            y='Count',
            color='Accepted taxon name',
            title=f"Macrofauna Graph {graph_type} {id['index'] + 1}: Avg. number of individuals per m² in survey {survey}",
        )
        fig.update_yaxes(title="Avg. individuals per m²")

    else:
        traces = []
        for sp in selected_taxa:
            LOGGER.info(f"Starting update chart for selected taxon: {sp}")
            sp_df = filtered_df[filtered_df['Accepted taxon name'] == sp].copy()
            if sp_df.empty:
                LOGGER.info(f"No data for selected taxon: {sp}")
                continue
            LOGGER.info(f"Done update chart for selected taxon: {sp}")
            if aggregation == 'sample':
                x_values = sp_df['Sampling datetime'].dt.date
                # Arbitrarily fix bar width to 30 days (expressed in milliseconds).
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True, width=1000*60*60*24*30)
            elif aggregation == 'year':
                sp_df = sp_df.groupby(
                    ['Year', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Year']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'month':
                sp_df = sp_df.groupby(
                    ['Month', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Month']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'month per year':
                sp_df = sp_df.groupby(
                    ['Year month', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Year month']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            elif aggregation == 'season':
                sp_df = sp_df.groupby(
                    ['Season', 'Accepted taxon name']).agg({'Count': 'mean'})
                sp_df = sp_df.reset_index()
                x_values = sp_df['Season']
                trace = go.Bar(x=x_values, y=sp_df['Count'], name=sp, showlegend=True)
            traces.append(trace)

        layout = go.Layout(
            title=f"Macrofauna Graph {graph_type} {id['index'] + 1}: Average number of individuals per m² in survey {survey} ",
            xaxis={'title': 'Sampling date'}, yaxis={'title': 'Avg. number of individuals per m²'})

        fig = go.Figure(data=traces, layout=layout)
        if aggregation == 'sample':
            fig.update_xaxes(type='date',range=[start_date,end_date])
        if aggregation == 'year':
            start_year = start_date.year
            end_year = end_date.year
            categories=[*range(start_year,end_year+1)]
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0,len(categories)])

        if aggregation == 'season' :
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=['Winter','Spring','Summer','Autumn'],
                             range=[-0.5, 3.5])
        if aggregation == 'month':
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:], range=[-0.5, 11.5])
        if aggregation == 'month per year':
            start_year = start_date.year
            end_year = end_date.year
            categories=[]
            for year in range(start_year,end_year+1):
                for month in range(1, 13):
                    categories.append(f"{year:04d}-{month:02d}")
            fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                             range=[0,len(categories)])

    return fig

@callback(
    Output({'type': 'granulometry-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'granulometry-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'granulometry-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'granulometry-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'granulometry-survey-aggregation', 'index': MATCH}, 'value'),
    Input({"type": 'granulometry-graph-type', "index": MATCH}, 'value'),
    State({'type': 'granulometry-survey-dropdown', 'index': MATCH}, 'id')
)
def update_granulometry_graph(selected_survey, start_date, end_date, aggregation, graph_type, id):
    """
    Update the granulometry graph based on the selected survey, date range, aggregation, and graph type.

    :param selected_survey: Selected survey value from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param id: ID of the granulometry survey dropdown.
    :return: Updated granulometry graph figure.
    """
    if start_date is None:
        start_date = datetime.date(1900, 1, 1)

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)

    if selected_survey is None:
        selected_survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = selected_survey
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

        if isinstance(selected_survey, str):
            selected_survey = [selected_survey]

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")

    dftmp = bdt.granulometry_df[
        (bdt.granulometry_df['Survey'].isin(selected_survey)) &
        (bdt.granulometry_df['Sampling date'] >= start_date) &
        (bdt.granulometry_df['Sampling date'] <= end_date)
        ]

    filtered_df = dftmp.copy()

    date_range = pd.date_range(start=start_date, end=end_date, freq='D')
    if aggregation == 'year':
        filtered_df['time_group'] = pd.to_datetime(filtered_df['Sampling date']).dt.year
        time_groups = pd.Series(date_range.year, index=date_range)
        time_format = '%Y'
    elif aggregation == 'month':
        filtered_df['time_group'] = pd.to_datetime(filtered_df['Sampling date']).dt.month.apply(
            lambda x: calendar.month_name[x])
        time_groups = pd.Series(date_range.month_name(), index=date_range)
        time_format = '%B'
    elif aggregation == 'month per year':
        filtered_df['time_group'] = pd.to_datetime(filtered_df['Sampling date']).dt.strftime('%Y-%m')
        time_groups = pd.Series(date_range.strftime('%Y-%m'), index=date_range)
        time_format = '%Y-%m'
    elif aggregation == 'season':
        time_groups = pd.Series(pd.cut(date_range, bins=4, labels=['Winter', 'Spring', 'Summer', 'Autumn']),
                                index=date_range)
        time_format = None
        filtered_df['Sampling date'] = pd.to_datetime(filtered_df['Sampling date'])
        filtered_df['time_group'] = filtered_df['Sampling date'].apply(lambda x: time_groups[x])
    elif aggregation == 'season per year':
        time_groups = pd.Series(pd.cut(date_range, bins=4, labels=['Winter', 'Spring', 'Summer', 'Autumn']),
                                index=date_range)
        time_format = '%Y - %B'
        filtered_df['Sampling date'] = pd.to_datetime(filtered_df['Sampling date'])
        filtered_df['time_group'] = filtered_df['Sampling date'].dt.year.astype(str) + ' - ' + filtered_df[
            'Sampling date'].apply(lambda x: time_groups[x])

    grouped_df = filtered_df.groupby(['time_group', 'Survey', 'Mesh size'])['Value'].mean().reset_index()

    # Convert the 'time_group' column to a string representation before concatenating
    if time_format is not None:
        grouped_df['time_group'] = grouped_df['time_group'].astype(str)

    # Création d'une liste unique de Mesh sizes pour la construction des barres empilées
    unique_mesh_sizes = grouped_df['Mesh size'].unique()
    unique_mesh_sizes = sorted(unique_mesh_sizes, key=functools.cmp_to_key(lambda a,b : int(re.sub(r'^\[(\d+),.*$',r'\1',a)) - int(re.sub(r'^\[(\d+),.*$',r'\1',b))))
    # Create the graph based on the selected graph type
    if graph_type == 'histogram':
        # Create the unstacked bar chart using plotly.graph_objects
        fig = go.Figure()

        for mesh_size in unique_mesh_sizes:
            # Filtrage des données pour le Mesh size actuel
            df_mesh_size = grouped_df[grouped_df['Mesh size'] == mesh_size]

            # Ajout d'une barre non empilée pour chaque Mesh size
            fig.add_trace(go.Bar(
                x=df_mesh_size['time_group'] + ', ' + df_mesh_size['Survey'],
                y=df_mesh_size['Value'],
                name=mesh_size,
                offsetgroup=mesh_size,  # Add this to unstack the bars
            ))


        fig.update_layout(
            barmode='group',
            xaxis_title='Sampling Date - Survey',
            yaxis_title='Weight of Fractions (%)',
            title=f"Granulometry Graph {graph_type} {id['index'] + 1}: Weight percentages of particle size fractions",
            legend_title='Mesh Size',
            xaxis_tickangle=-45,
        )
    elif graph_type == 'stacked histogram':

        fig = go.Figure()

        for mesh_size in unique_mesh_sizes:
            # Filtrage des données pour le Mesh size actuel
            df_mesh_size = grouped_df[grouped_df['Mesh size'] == mesh_size]

            # Ajout d'une barre empilée pour chaque Mesh size
            fig.add_trace(go.Bar(
                x=df_mesh_size['time_group'] + ', ' + df_mesh_size['Survey'],
                y=df_mesh_size['Value'],
                name=mesh_size,
            ))

        # Mise en forme du layout
        fig.update_layout(
            barmode='stack',
            xaxis_title='Sampling Date - Survey',
            yaxis_title='Weight of Fractions (%)',
            title=f"Granulometry Graph {graph_type} {id['index'] + 1}: Weight percentages of particle size fractions",
            legend_title='Mesh Size',
            xaxis_tickangle=-45,
            legend_traceorder='normal',
        )

    elif graph_type == 'pie':
        # Group the dataframe by taxon name and sum the counts
        pie_data = filtered_df.groupby(['Mesh size', 'Survey'], as_index=False)['Value'].mean()

        # Calculate the number of survey
        num_survey = len(pie_data['Survey'].unique())

        # Calculate the height based on the number of survey (adjust the multiplier as per your preference)
        fig_height = 300 + 200 + (num_survey/3 * 200)

        fig = px.pie(
            pie_data,
            names='Mesh size', values='Value',
            title=f"Granulometry Graph {graph_type} {id['index'] + 1}: Weight percentages of particle size fractions",
            facet_col='Survey',
            facet_col_wrap=min(num_survey, 3),
            height=fig_height,  # Set the height of the figure dynamically
            category_orders={'Mesh size': unique_mesh_sizes},
        )
        fig.update_traces(textposition='inside')
        fig.update_layout(
            legend_title_text='Mesh size',
            legend=dict(
                orientation='h',  # Place the legend below the figure horizontally
                yanchor='bottom',  # Anchor the legend to the bottom of the figure
#                y=-0.1,  # Position the legend at the bottom of the figure (y=1.0 is at the bottom)
                xanchor='center',  # Anchor the legend to the center of the figure
                x=0.5  # Align the legend to the center of the figure
            )
        )

    return fig


@callback(
    Output({'type': 'top_taxon-graph', 'index': MATCH}, 'figure'),
    Input({'type': 'top-taxon-survey-dropdown', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-graph-type', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-survey-date-picker', 'index': MATCH}, 'start_date'),
    Input({'type': 'top-taxon-survey-date-picker', 'index': MATCH}, 'end_date'),
    Input({'type': 'top-taxon-survey-aggregation', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-computation-method', 'index': MATCH}, 'value'),
    Input({'type': 'top-taxon-input', 'index': MATCH}, 'value'),
    State({'type': 'top-taxon-input', 'index': MATCH}, 'id')

)
def update_top_taxon_graph(survey, graph_type, start_date, end_date, aggregation, computation_method, top_taxon_input, id):
    """
    Update the top taxon graph based on the selected survey, graph type, date range, aggregation, and top taxon count.

    :param survey: Selected survey value from the dropdown.
    :param graph_type: Graph type selected from the dropdown.
    :param start_date: Start date selected from the date picker.
    :param end_date: End date selected from the date picker.
    :param aggregation: Aggregation type selected from the dropdown.
    :param top_taxon_input: Top taxon count input value.
    :param id: ID of the top taxon survey dropdown.
    :return: Updated top taxon graph figure.
    """

    LOGGER.debug(f"Updating top taxon graph ")
    if top_taxon_input is None:
        top_taxon_input = []

    if start_date is None:
        start_date = datetime.date(1900, 1, 1)
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d").date()

    if end_date is None:
        end_date = datetime.date(2900, 12, 31)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d").date()

    LOGGER.info(f"Date type: {type(start_date)}, {start_date}")

    if survey is None:
        survey = ''

    control_index = id['index']
    if f"index-{control_index}" in controls_by_index:
        controls_by_index[f"index-{control_index}"]['survey'] = survey
        controls_by_index[f"index-{control_index}"]['top_taxon'] = top_taxon_input
        controls_by_index[f"index-{control_index}"]['start_date'] = start_date
        controls_by_index[f"index-{control_index}"]['end_date'] = end_date
        controls_by_index[f"index-{control_index}"]['graph_type'] = graph_type
        controls_by_index[f"index-{control_index}"]['aggregation'] = aggregation

    else:
        LOGGER.debug(f"Unable to update control values at index {control_index}")
    filtered_df = bdt.macrofauna_df[
        (bdt.macrofauna_df['Survey'] == survey) &
        (bdt.macrofauna_df['Sampling datetime'].dt.date >= start_date) &
        (bdt.macrofauna_df['Sampling datetime'].dt.date <= end_date)
    ]

    time_group = 'Sampling date'
    if aggregation == 'year':
        time_group = 'Year'
    elif aggregation == 'month':
        time_group = 'Month'
    elif aggregation == 'month per year':
        time_group = 'Year month'
    elif aggregation == 'season':
        time_group = 'Season'


    # Determine the most abundant taxons (cumulative or average) in the given survey and time interval
    if computation_method == 'cumulative':
        top_taxons_df = filtered_df.groupby('Accepted taxon name',as_index=False)['Count'].sum()
    if computation_method == 'average':
        top_taxons_df = filtered_df.groupby('Accepted taxon name', as_index=False)['Count'].mean()
        
    top_taxons_df.sort_values(by=['Count'],inplace=True,ascending=False)
    top_taxons = top_taxons_df.iloc[0:top_taxon_input]['Accepted taxon name'].tolist()

    # Build a dataframe containing only data for the most abundant taxons.
    top_taxons_df = filtered_df[filtered_df['Accepted taxon name'].isin(top_taxons)].copy()

    # Build a dataframe for taxons not belonging to the topmost taxons.
    other_taxons_df = filtered_df[~filtered_df['Accepted taxon name'].isin(top_taxons)].copy()
    # Force their taxon names to 'Others'
    other_taxons_df['Accepted taxon name'] = 'Others'
    # Group the 'Others' taxons by sampling date
    other_taxons_df =other_taxons_df.groupby([time_group,'Accepted taxon name'])['Count'].mean()
    other_taxons_df =other_taxons_df.reset_index()
    # Build the dataframe to be used for the graph by combining the dataframe with
    # with the topmost taxa counts and the dataframe with the 'Others' counts.
    filtered_df = pd.concat([top_taxons_df, other_taxons_df])

    if graph_type == 'pie':
        fig = px.pie(filtered_df, names='Accepted taxon name', labels={'Accepted taxon name': 'Accepted taxon name'},
                     title=f"Top Graph {graph_type} {id['index'] + 1}: Top {top_taxon_input} taxons in survey {survey}")
        fig.update_layout(legend_title_text=(f"Top {top_taxon_input} Taxons"),
                          title="Avg. individuals per m²")

    elif graph_type == 'box':
        fig = px.box(
            filtered_df,
            x=time_group,
            y='Count',
            color='Accepted taxon name',
            title=f"Top Graph {graph_type} {id['index'] + 1}: Top {top_taxon_input} taxons in survey {survey}",
        )
        fig.update_yaxes(title="Avg. individuals per m²")
        fig.update_layout(
            xaxis=dict(title='Sampling Date',),
            legend_title_text=(f"Top {top_taxon_input} Taxons")
        )

    elif graph_type == 'histogram':
        fig = px.histogram(filtered_df, x=time_group, y='Count', color='Accepted taxon name',
                           histfunc="avg",
                           labels={'Accepted taxon name': 'Accepted taxon name'},
                           title=f"Top Graph {graph_type} {id['index'] + 1}: Top {top_taxon_input} taxons in survey {survey}" )
        fig.update_layout(
            xaxis=dict(title='Sampling Date',),
            yaxis=dict(title='Avg. individuals per m²'),
            legend_title_text=(f"Top {top_taxon_input} Taxons"),
            barmode='group'  # Modifier le barmode pour obtenir un histogramme non empilé
        )

    elif graph_type == 'stacked histogram':
        fig = px.histogram(filtered_df, x=time_group, y='Count', color='Accepted taxon name',
                           histfunc="avg",
                           barmode='stack', labels={'Accepted taxon name': 'Accepted taxon name'},
                           title=f"Top Graph {graph_type} {id['index'] + 1}: Top {top_taxon_input} taxon in survey {survey}" )
        fig.update_yaxes(title="Avg. individuals per m²")
        fig.update_layout(
            xaxis=dict(title='Sampling Date', ),
            legend_title_text=(f"Top {top_taxon_input} Taxons"),
            barmode='stack',
            bargap=0.2
        )

    if aggregation == 'sample':
        fig.update_xaxes(type='date', range=[start_date, end_date])
    if aggregation == 'year':
        start_year = start_date.year
        end_year = end_date.year
        categories = [*range(start_year, end_year + 1)]
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                         range=[0, len(categories)])

    if aggregation == 'season':
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=['Winter', 'Spring', 'Summer', 'Autumn'],
                         range=[-0.5, 3.5])
    if aggregation == 'month':
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=list(calendar.month_name)[1:],
                         range=[-0.5, 11.5])
    if aggregation == 'month per year':
        start_year = start_date.year
        end_year = end_date.year
        categories = []
        for year in range(start_year, end_year + 1):
            for month in range(1, 13):
                categories.append(f"{year:04d}-{month:02d}")
        fig.update_xaxes(type='category', categoryorder='array', categoryarray=categories,
                         range=[0, len(categories)])

    return fig

graph_control_tabs = dbc.Tabs(id="graph-controls", children=[], active_tab='tab-0')

layout = html.Div([
    html.Div([
        html.Div([
                dbc.Button(
                    children=[html.I("", className="bi bi-plus"), html.Span("Macrofauna")],
                    id="add-macrofauna-graph-button",
                    n_clicks=0,
                    className="btn btn-success"
                ),

                dbc.Button(
                    children=[html.I("", className="bi bi-plus"), html.Span("Granulometry")],
                    id="add-granulometry-graph-button",
                    n_clicks=0,
                    className="btn btn-success"
                ),

                dbc.Button(
                    children=[html.I("", className="bi bi-plus"), html.Span("Top Taxa")],
                    id="add-top-taxon-graph-button",
                    n_clicks=0,
                    className="btn btn-primary"
                )]),
        html.Div([
            graph_control_tabs,
            html.Div([
                dbc.Button(
                    "Remove",
                    id="remove-graph-button",
                    n_clicks=0,
                    disabled=True,
                    color="danger",
                    className="bd-control-button",
                ),
                dbc.Button(
                    "Duplicate",
                    id='duplicate-graph-button',
                    color="secondary",
                    className="bd-control-button",
                ),
                ], className="bd-control-buttons")
        ], className="bd-controls"),
    ], id='navbar', className="bd-navbar"),

    # Main content
    html.Div([
        html.Details(
            open=True,
            children=[
                html.Summary("BenthOBS SNO Sampling Stations Maps"),
                dl.Map([
                    dl.TileLayer(),
                    dl.LayerGroup(id='marker-layer'),
                    dl.FullscreenControl(position='topleft'),
                    dl.LayersControl(
                        [
                            dl.BaseLayer(dl.TileLayer(
                                url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"),
                                         name='Esri WorldImagery', checked=True),
                            dl.BaseLayer(dl.TileLayer(), name='Base Map'),
                            dl.BaseLayer(dl.TileLayer(url="https://tile.openstreetmap.bzh/br/{z}/{x}/{y}.png"),
                                         name='OpenStreetMap'),
                            dl.BaseLayer(dl.TileLayer(
                                url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"),
                                         name='Esri WorldTopoMap'),
                            dl.Overlay(dl.MarkerClusterGroup(id='survey-info-layer'), name='Survey Information', checked=True),
                            dl.Overlay(dl.MarkerClusterGroup(id='taxon-layer'), name='Top 5 Taxons', checked=False)
                        ]
                    )
                ],
                    center=(46.59493039157112, 2.4178988109021606),
                    style={'max-width': '100%', 'height': '400px', 'position': 'center'},
                    zoom=5,
                )]),
        html.Hr(className='bd-hr'),
        html.Div(id="graph-panels", children=[]),
        html.Div(id='marker-popup', style={'display': 'none'}),
    ], className='bd-content'),
])
