window.addEventListener("load", (event) => {
    let tracker_url = document.querySelector('meta[property="matomo-tracker-url"]').content;
    let site_id = document.querySelector('meta[property="matomo-site-id"]').content;
    if (tracker_url != null && tracker_url != "" && site_id != null && site_id != "") {
        let _paq = window._paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            let u=tracker_url;
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', site_id]);
            let d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
    }
});

