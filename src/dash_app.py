from dash import clientside_callback, Dash, html, Input, Output
import dash
import dash_bootstrap_components as dbc
import toml
import argparse

import benthobs_data_tools as bdt
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


def build_app(config):
    """
    Build the BenthOBS Dash application.

    :param config: Configuration dictionary containing web application settings.
    :type config: dict

    :return: The constructed Dash application.
    """

    matomo_tracker_url = ''
    matomo_site_id = ''
    if 'matomo' in config and 'tracker_url' in config['matomo'] and 'site_id' in config['matomo']:
        LOGGER.debug(
            f"Initializing Matomo collector with tracker url {config['matomo']['tracker_url']} and site id {config['matomo']['site_id']}")
        matomo_tracker_url = config['matomo']['tracker_url']
        matomo_site_id = config['matomo']['site_id']
    else:
        LOGGER.debug(f"Matomo configuration missing or incomplete. Skipping.")

    app = Dash('benthobsdash',
               title="BenthOBS SNO Data Viewer",
               external_stylesheets=[dbc.themes.YETI, dbc.icons.BOOTSTRAP],
               meta_tags=[
                   {"name": "viewport", "content": "width=device-width, initial-scale=1"},
                   {"property": "matomo-tracker-url", "content": matomo_tracker_url},
                   {"property": "matomo-site-id", "content": matomo_site_id},
               ],
               use_pages=True,
               **config['webapp']
               )

    app.layout = html.Div([
        html.Div(className="bd-background"),
        html.H1("BenthOBS SNO Data Viewer", className="h1 bd-title font-weight-bolder"),
        html.Div(
            dbc.Nav(
            [
                dbc.NavItem(
                    dbc.NavLink(
                        f"{page['name']}", href=page["relative_path"], className="bd-navbar-text-light",
                    )
                )
                for page in dash.page_registry.values()
            ], pills=True, fill=True),
            className="bd-top-sections navbar navbar-dark bg-dark"
            ),
        html.A(
            html.Img(
                src='assets/images/ir-ilico_logo.png',
                className='bd-ilicologo'
            ),
            href='https://www.ir-ilico.fr/?PagePrincipale',
            target='_blank'
        ),
        html.A(
            html.Img(
                src='assets/images/Benthobs.png',
                className='bd-benthobslogo'
            ),
            href="https://www.benthobs.fr",
            target='_blank'
        ),
        dash.page_container
        ],
    )

    return app


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog='BenthOBS Dashboard'
    )
    parser.add_argument('config', type=open)

    args = parser.parse_args()

    config = toml.load(args.config)

    (bdt.macrofauna_df, bdt.granulometry_df) = bdt.load_data(config)
    bdt.compute_most_abundant_taxa()
    app = build_app(config)
    app.run_server(**config['server'])
