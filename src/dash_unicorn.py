import toml

import benthobs_data_tools as bdt
import dash_app

config = toml.load('/config/benthobsdash_config.toml')
(bdt.macrofauna_df, bdt.granulometry_df) = bdt.load_data(config)
bdt.compute_most_abundant_taxa()

application = dash_app.build_app(config)

server = application.server

