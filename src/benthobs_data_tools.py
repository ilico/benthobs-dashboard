import logging
import os.path
import pandas as pd
import plotly.colors as colors

survey_taxons = {}
macrofauna_df = None
granulometry_df = None

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

def load_data(config):
    """
    Load data from specified URLs or cached files.

    :param config: Configuration dictionary containing URLs and file paths for data.
    :type config: dict

    :return: Tuple containing the loaded macrofauna and granulometry dataframes.
    :rtype: tuple
    """
    MACROFAUNA_URL = config['macrofauna']['url']
    MACROFAUNA_SEP = config['macrofauna']['separator']
    CACHED_MACROFAUNA_FILENAME = config['macrofauna']['file']

    GRAULOMTERY_URL = config['granulometry']['url']
    GRANULOMETRY_SEP = config['granulometry']['separator']
    CACHED_GRANULOMETRY_FILENAME = config['granulometry']['file']

    macrofauna_file_abspath = os.path.abspath(CACHED_MACROFAUNA_FILENAME)
    macrofauna_file_dirname = os.path.dirname(macrofauna_file_abspath)
    if not os.path.exists(macrofauna_file_dirname):
        os.makedirs(macrofauna_file_dirname)

    granulometry_file_abspath = os.path.abspath(CACHED_GRANULOMETRY_FILENAME)
    granulometry_file_dirname = os.path.dirname(granulometry_file_abspath)
    if not os.path.exists(granulometry_file_dirname):
        os.makedirs(granulometry_file_dirname)

    if os.path.exists(CACHED_MACROFAUNA_FILENAME):
        LOGGER.debug("Loading macrofauna data from local file.")
        m_df = pd.read_pickle(CACHED_MACROFAUNA_FILENAME)
    else:
        LOGGER.debug("Loading macrofauna data from URL.")
        full_m_df = pd.read_csv(MACROFAUNA_URL,
                              sep=MACROFAUNA_SEP,
                              low_memory=False)
        m_df = full_m_df.filter(
            ['Survey', 'Latitude', 'Longitude', 'Sampling date', 'Accepted taxon name', 'Count', 'Sample size'])
        # Normalize count units to individuals per square m.
        m_df['Count'] = m_df['Count']/m_df['Sample size']
        m_df = m_df.drop(columns=['Sample size'])
        # Pool replicates and take average of counts.
        m_df = m_df.groupby(
            ['Survey', 'Latitude', 'Longitude', 'Sampling date', 'Accepted taxon name']).agg({'Count': 'mean'})
        m_df = m_df.reset_index()

        m_df['Coordinates'] = list(zip(m_df['Latitude'], m_df['Longitude']))
        m_df['Accepted taxon name'] = m_df['Accepted taxon name'].apply(str)
        m_df.replace(to_replace={'Accepted taxon name': r"^(.*)$"}, value={'Accepted taxon name': r"<i>\1</i>"},
                            regex=True, inplace=True)

        # Create additional columns used for aggregated bar charts.
        m_df["Sampling datetime"] = pd.to_datetime(m_df['Sampling date'])
        m_df['Year'] = m_df['Sampling datetime'].dt.year
        m_df['Month'] = m_df['Sampling datetime'].dt.month_name()
        m_df['Year month'] = (m_df['Sampling datetime'].dt.year).astype(str) + '-' + (m_df['Sampling datetime']).dt.strftime("%m")
        m_df['Date offset'] = (m_df['Sampling datetime'].dt.month * 100 + m_df['Sampling datetime'].dt.day - 320) % 1300
        m_df['Season'] = pd.cut(m_df['Date offset'], [0, 300, 602, 900, 1300],
                              labels=['Spring', 'Summer', 'Autumn', 'Winter'])

        m_df.to_pickle(CACHED_MACROFAUNA_FILENAME)

    if os.path.exists(CACHED_GRANULOMETRY_FILENAME):
        LOGGER.debug("Loading granulometry data from local file.")
        g_df = pd.read_pickle(CACHED_GRANULOMETRY_FILENAME)
    else:
        LOGGER.debug("Loading granulometry data from URL.")
        full_granulometry_df = pd.read_csv(GRAULOMTERY_URL,
                                           sep=GRANULOMETRY_SEP,
                                           low_memory=False)
        g_df = full_granulometry_df.filter(
            ['Survey', 'Latitude', 'Longitude', 'Depth', 'Sampling date', 'Value', 'Unit', 'Mesh size'])
        g_df.to_pickle(CACHED_GRANULOMETRY_FILENAME)

    return (m_df, g_df)


def compute_most_abundant_taxa():
    """
    Compute the most abundant taxons for each survey.

    This function calculates the most abundant taxons for each survey based on the 'Count' column in the
    'macrofauna_df' dataframe. It groups the data by 'Survey', calculates the mean count for each taxon,
    selects the top 5 taxons, and creates an 'Others' category to hold the mean count of all other taxons.

    The computed results are stored in the 'survey_taxons' dictionary.

    :return:
    """
    LOGGER.info("Starting topmost taxon computation.")
    for survey in macrofauna_df['Survey'].unique():
        survey_data = macrofauna_df[macrofauna_df['Survey'] == survey]
        top_taxons = survey_data.groupby('Accepted taxon name')['Count'].mean().nlargest(5)
        others_count = survey_data[~survey_data['Accepted taxon name'].isin(top_taxons.index)]['Count'].mean()
        top_taxons = top_taxons.reset_index()
        top_taxons.loc[len(top_taxons)] = ['Others', others_count]
        # Générer des couleurs pour les taxons
        num_taxons = len(top_taxons)
        colorscale = colors.qualitative.Dark24[:num_taxons]
        top_taxons['Color'] = colorscale
        survey_taxons[survey] = top_taxons
    LOGGER.info("Done topmost taxon computation.")


